// require the module
const be = require('blockexplorer');
const { promisify } = require('util');
const fs = require('fs');
const bitcoin = require('./bitcoin.json');

async function scanWallets (blockHash) {
    let wallets = [];
    const { tx, nextblockhash } = JSON.parse(await be.block(blockHash));
    for (let id of tx) {
        const { vout, vin } = JSON.parse(await be.tx(id));
        console.log('TX: ' + id);
        for (let out of vout) {
            wallets = wallets.concat(out.scriptPubKey.addresses);
        }
    }
    return {
        nextblockhash,
        wallets
    };
}

async function loop (){
    try {
        const { nextblockhash, wallets }  =  await scanWallets(bitcoin.nextHash);
        bitcoin.nextBlock ++;
        bitcoin.nextHash = nextblockhash;
        bitcoin.wallets = Array.from(new Set(bitcoin.wallets.concat(wallets)))
        await promisify(fs.writeFile)('./bitcoin.json', JSON.stringify(bitcoin, undefined, 4), 'utf8');
        console.log('SCANED ' + (bitcoin.nextBlock - 1) + ' ...');
        setTimeout(loop, 2000);
    } catch (e) {
        console.log('Retry ...');
        setTimeout(loop, 5000);
    }
}

setTimeout(loop, 1000);
